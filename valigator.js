/* Validation object end - WORK IN PROGRESS!!!!! */

function Valigator(){
	this.valid = {};
	this.error = {};
	this.valid.all = 1;
	this.toValidate = {};
	this.errorOutputs = [];

	this.addCondition = function(inputName, condName, condValue){

		// If this input is not in the validation array then add it
		if(!this.toValidate[inputName]){
			this.toValidate[inputName] = {};
		}

		this.toValidate[inputName][condName] = condValue;
		this.valid[inputName] = 1;
	}

	/*
	* inputName - type: string
	* error - type: function
	*/
	this.setError = function(inputName, error){
		//TODO - Check that error type is function

		this.error[inputName] = error;
	}
	

	this.validate = function(){
		if(typeof(this.toValidate) == 'object'){

			for(var input in this.toValidate){

				var thisInput = this.toValidate[input];

				for(var condition in thisInput){
					switch (condition) {
					case 'maxLength':
						this.maxLength(input,thisInput[condition]);
						break;
					case 'maxLengthOpt':
						this.maxLengthOpt(input,thisInput[condition]);
						break;
					case 'minLength':
						this.minLength(input,thisInput[condition]);
						break;
					case 'email':
						this.email(input);
						break;
					case 'regex':
						this.regex(input,thisInput[condition]);
						break;
					}
				}

				this.checkValid(input);
			}

			if(this.valid.all == 1){
				return {'valid': 1};
			} else {
				return {'valid': 0, 'errors': this.errorOutputs};
			}
		}
	}

	this.maxLength = function(input,compare){
		var thisJqObj = $('[name=' + input + ']');
		var thisVal = thisJqObj.val();
		
		if(thisVal.length > compare){
			this.invalidate(input);
		}
	}
	this.maxLengthOpt = function(input,compare){
		var thisJqObj = $('[name=' + input + ']');
		var thisVal = thisJqObj.val();

		if( thisVal.length > compare &&
			thisVal.length > 0 ){
			this.invalidate(input);
		}
	}

	this.minLength = function(input,compare){
		var thisJqObj = $('[name=' + input + ']');
		var thisVal = thisJqObj.val();
		
		if(thisVal.length < compare){
			this.invalidate(input);
		}
		
	}
	this.minLengthOpt = function(input,compare){
		var thisJqObj = $('[name=' + input + ']');
		var thisVal = thisJqObj.val();
		
		if(thisVal.length < compare &&
			thisVal.length > 0 ){
			this.invalidate(input);
		}
		
	}

	this.email = function(input){
		var thisJqObj = $('[name=' + input + ']');
		var thisVal = thisJqObj.val();
		
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(!re.test(thisVal)){
			this.invalidate(input);
		}
		
	}

	this.regex = function(input, regex){

		/* Checks if REGEX is valid */
		var validRegex;
		try { 
		    regex = RegExp(regex);
		    validRegex = true;
		}
		catch(e) {
		    validRegex = false;
		}

		if(validRegex){
			var thisJqObj = $('[name=' + input + ']');
			var thisVal = thisJqObj.val();

			if(!regex.test(thisVal)){
				this.invalidate(input);
			}
		} else {
			this.invalidate(input);
			this.errorOutputs.push('The RegEx validation on ' + input + ' has failed as the RegEx entered seems to be invalid.');
		}
	}

	this.invalidate = function(input){
		this.valid.all = 0;
		this.valid[input] = 0;
	}
	
	this.checkValid = function(input){
		if(this.valid[input] == 0){
			if(this.error[input]){
				this.error[input]();

				this.errorOutputs.push(input + ' validation has failed.');
			} else {
				//If no error condition is set
				this.errorOutputs.push(input + ' validation has failed.');
			}
		}
	}
}

/* Validation object end - WORK IN PROGRESS!!!!! */